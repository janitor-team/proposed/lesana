The following features are already planned.

The following features may be nice to have.

* Having an indexing mode that adds the field as a value instead of a
  term, see
  https://getting-started-with-xapian.readthedocs.io/en/latest/concepts/indexing/values.html
