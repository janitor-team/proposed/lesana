#########################
 Developer Documentation
#########################

Documentation that is useful for developers who are using lesana as a
library.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   promises

