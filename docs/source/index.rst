.. lesana documentation master file, created by
   sphinx-quickstart on Thu Oct  1 22:28:26 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.rst

Documentation
-------------

The documentation for the latest development version of lesana can be
browsed online at https://lesana.trueelena.org; `PDF
<https://lesana.trueelena.org/lesana.pdf>`_ and `epub
<https://lesana.trueelena.org/lesana.epub>`_ versions are also
available [#onion]_.

.. [#onion] Everything is also available via onion, at
   http://aublvconhsld6cvcf3dbibffzih2un5bicp3s3b5qmkskof26p3pssqd.onion/

The author can be contacted via email: webmaster AT trueelena DOT org.

.. only:: html

   Status Badges
   -------------

   Packaging
   ^^^^^^^^^

   .. image:: https://repology.org/badge/vertical-allrepos/lesana.svg
      :target: https://repology.org/project/lesana/versions

   CI
   ^^

   .. image:: https://builds.sr.ht/~valhalla/lesana.svg
      :target: https://builds.sr.ht/~valhalla/lesana

Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   user/index
   devel/index
   contrib/index

   man/index

   reference/modules

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
