##############
 My Bookmarks
##############

This is an example collection of bookmarks.

A page with all the bookmarks can be generated with the command::

   lesana search --all --template templates/page.html '*' > my_bookmarks.html
