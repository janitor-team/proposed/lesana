from .collection import Collection, Entry, TemplatingError

# prevent spurious warnings from pyflakes
assert Collection
assert Entry
assert TemplatingError
